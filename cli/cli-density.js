const fs = require("fs-extra");
const _ = require("lodash");
const { keywordDensity } = require("../src/keyword-density");
const gp = require("google-play-scraper");
const csvStringify = require("csv-stringify");
const ProgressBar = require("progress");
const path = require("path");
const sanitize = require("sanitize-filename");

exports.command = "density [appIds..]";
exports.description =
  "Рассчитывает частоту появления ключевых слов в данных приложениях";

exports.builder = yargs =>
  yargs
    .positional("appIds", {
      description: "Id приложений, которые необходимо обработать"
    })
    .option("appList", {
      description: "Файл со списком приложений",
      type: "string"
    })
    .option("merge", {
      alias: "m",
      description:
        "Нужно ли объединять результаты по всем приложениям в один файл;",
      type: "boolean"
    })
    .option("output", {
      alias: "o",
      description: "Файл для вывода; требует, чтобы был указан флаг -m",
      implies: "merge",
      type: "string"
    })
    .option("outputDir", {
      alias: "d",
      description: "Папка для вывода результатов по приложениям",
      conflicts: "merge",
      type: "string",
      default: "."
    })
    .option("maxWords", {
      alias: "w",
      description: "Максимальное количество слов в ключевике",
      type: "number",
      default: 4
    })
    .option("minFrequency", {
      description: "Минимальная частота ключевика",
      type: "number",
      default: 1
    })
    .option("minPhraseLength", {
      description:
        "Минимальная длина фразы (кол-во символов); полезно для отсечения всяких предлогов",
      type: "number",
      default: 4
    });

exports.handler = async argv => {
  /**
   * @type {string[]}
   */
  const appIds = [];

  appIds.push(...argv.appIds);

  if (argv.appList) {
    const lines = fs
      .readFileSync(argv.appList)
      .toString()
      .split("\n")
      .map(line => line.trim())
      .filter(line => line.length > 0);

    appIds.push(...lines);
  }

  const progess = new ProgressBar(
    "[:current/:total] [:bar] :percent | :elapseds elapsed; ETA: :etas",
    {
      total: appIds.length,
      width: 30
    }
  );

  const densityOptions = {
    maxWords: argv.maxWords
  };

  const appFrequencies = [];

  for (const appId of appIds) {
    const app = await gp.app({
      appId,
      lang: argv.hl,
      country: argv.gl,
      throttle: argv.throttle
    });
    const frequencies = appToUnflattenedFrequencies(app, densityOptions);
    appFrequencies.push(frequencies);
    progess.tick();
  }

  const processFrequenciesOptions = {
    minFrequency: argv.minFrequency,
    minPhraseLength: argv.minPhraseLength
  };

  if (argv.merge) {
    // merge way
    const frequenciesList = processFrequencies(
      _.concat(appFrequencies),
      processFrequenciesOptions
    );
    outputFrequenciesToCsv(
      frequenciesList,
      argv.output || "apps-keyword-density.csv"
    );
  } else {
    // individual files for apps way
    _.zip(appIds, appFrequencies).forEach(([appId, appFrequency]) => {
      const frequenciesList = processFrequencies(
        appFrequency,
        processFrequenciesOptions
      );
      const filename = sanitize(`${appId}.csv`);
      outputFrequenciesToCsv(
        frequenciesList,
        path.join(argv.outputDir, filename)
      );
    });
  }
};

/**
 * @typedef {Object} Density
 * @property {string} Density.keyword
 * @property {number} Density.titleCount
 * @property {number} Density.shortDescCount
 * @property {number} Density.fullDescCount
 */

/**
 * Takes google-play-scraper App object and destructures it into all possible keywords.
 * @param {Object} app
 * @param {Object} densityOptions
 * @returns {Density[]}
 */
function appToUnflattenedFrequencies(app, densityOptions) {
  const densityTitle = keywordDensity(app.title, densityOptions).map(item => ({
    keyword: item.element,
    titleCount: item.frequency,
    shortDescCount: 0,
    fullDescCount: 0
  }));

  const densityShortDesc = keywordDensity(app.summary, densityOptions).map(
    item => ({
      keyword: item.element,
      titleCount: 0,
      shortDescCount: item.frequency,
      fullDescCount: 0
    })
  );

  const densityFullDesc = keywordDensity(app.description, densityOptions).map(
    item => ({
      keyword: item.element,
      titleCount: 0,
      shortDescCount: 0,
      fullDescCount: item.frequency
    })
  );

  return _.concat(densityTitle, densityShortDesc, densityFullDesc);
}

/**
 * @typedef {Object} FrequencyResult
 * @property {string} FrequencyResult.keyword
 * @property {number} FrequencyResult.wordCount
 * @property {number} FrequencyResult.titleCount
 * @property {number} FrequencyResult.shortDescCount
 * @property {number} FrequencyResult.fullDescCount
 * @property {number} FrequencyResult.totalCount
 */

/**
 * Reduces duplicates and adds some properties
 * @param {Density[]} appFrequencies
 * @param {object} options
 * @param {number} options.minFrequency
 * @param {number} options.minPhraseLength
 * @returns {FrequencyResult[]}
 */
function processFrequencies(appFrequencies, options) {
  // flatten
  const reducedKeywords = appFrequencies.reduce((acc, current) => {
    const accFreq = _.get(acc, current.keyword, {
      keyword: current.keyword,
      titleCount: 0,
      shortDescCount: 0,
      fullDescCount: 0
    });

    accFreq.titleCount += current.titleCount;
    accFreq.shortDescCount += current.shortDescCount;
    accFreq.fullDescCount += current.fullDescCount;
    acc[current.keyword] = accFreq;

    return acc;
  }, {});

  /**
   * @type {FrequencyResult[]}
   */
  const frequenciesList = _.values(reducedKeywords)
    .map(item => {
      item.totalCount =
        item.titleCount + item.shortDescCount + item.fullDescCount;

      item.wordCount = item.keyword.split(" ").length;

      return item;
    })
    .filter(item => {
      return (
        item.totalCount >= options.minFrequency &&
        item.keyword.length >= options.minPhraseLength
      );
    });

  return frequenciesList;
}

/**
 *
 * @param {FrequencyResult[]} frequenciesList
 * @param {string} outputPath
 */
function outputFrequenciesToCsv(frequenciesList, outputPath) {
  csvStringify(
    frequenciesList,
    {
      header: true,
      columns: [
        { key: "keyword", header: "Keyword" },
        { key: "wordCount", header: "Word count" },
        { key: "titleCount", header: "Title occurences" },
        { key: "shortDescCount", header: "Short description occurences" },
        { key: "fullDescCount", header: "Full description occurences" },
        { key: "totalCount", header: "Total occurences" }
      ]
    },
    (err, str) => {
      if (err) {
        console.error(err);
        return;
      }

      fs.outputFile(outputPath, str);
    }
  );
}
