const fs = require("fs-extra");
const sanitize = require("sanitize-filename");
const _ = require("lodash");
const miss = require("mississippi");
const split = require("split");
const csvStringify = require("csv-stringify");
const ProgressBar = require("progress");
const { AppSearchPositionDuplex } = require("../src/app-position");
const { countLinesInFile } = require("../src/util");

exports.command = "position <filename>";
exports.describe = "Check app position in search for a given list of keywords";
exports.builder = yargs =>
  yargs
    .option("app", {
      alias: ["apps", "package-name"],
      describe:
        "Package name in Google Play (can be multiple, either via `--app=APP1 --app=APP2`, or `--app APP1 APP2 --`)",
      requiresArg: true,
      type: "array"
    })
    .demandOption("app")
    .option("output", {
      alias: "o",
      describe:
        "Where to output positions list; if not specified, data is output into `./<package-name>-<hl>-<gl>.csv`",
      requiresArg: true,
      type: "string"
    });

exports.handler = async argv => {
  // подчистка и фильтр невалидных строк
  const trimmer = miss.through.obj((chunk, enc, cb) => {
    const line = chunk.toString().trim();
    if (line.length === 0) {
      cb();
    } else {
      cb(null, line);
    }
  });

  // проверка позиций
  const positionChecker = new AppSearchPositionDuplex(argv.apps, {
    country: argv.gl,
    lang: argv.hl
  });

  // TODO move to `gauge` package
  const progressBar = new ProgressBar(
    "[:current/:total] [:bar] :percent | :elapseds elapsed; ETA: :etas",
    {
      total: await countLinesInFile(
        argv.filename,
        line => line.trim().length > 0
      ),
      width: 30,
      stream: process.stdout
    }
  );
  const progressBarStream = miss.through.obj((chunk, enc, cb) => {
    progressBar.tick();
    cb(null, chunk);
  });

  // подготовка значений к записи в нужном формате
  const formatConverterPrepare = miss.through.obj((chunk, enc, cb) => {
    const nextChunk = {
      keyword: chunk.keyword
    };

    chunk.results.forEach(result => {
      nextChunk[result.appId] = result.isFound ? result.position : "";
    });

    cb(null, nextChunk);
  });

  // конвертер объектов в нужный формат файла
  const formatConverter = csvStringify({
    header: true,
    columns: _.concat(
      [
        { key: "keyword", header: "Keyword" },
      ],
      argv.apps.map(appId => ({ key: appId, header: `${appId} rank` }))
    )
  });

  // входной поток
  const input = fs.createReadStream(argv.filename);

  // придумываем название выходного файла
  let outputFileName;
  if (argv.output) {
    const filename = sanitize(argv.output);
    fs.ensureFileSync(filename);
    outputFileName = filename;
  } else if (argv.apps.length === 1) {
    outputFileName = `${argv.apps[0]}-${argv.gl}.csv`;
  } else {
    // TODO придумать обработку для случая, когда приложений несколько
    outputFileName = `app-rankings-${argv.gl}.csv`;
  }

  // выходной поток
  const output = fs.createWriteStream(outputFileName);

  input
    .pipe(split())
    .pipe(trimmer)
    .pipe(positionChecker)
    .pipe(progressBarStream)
    .pipe(formatConverterPrepare)
    .pipe(formatConverter)
    .pipe(output);

  return;
};
