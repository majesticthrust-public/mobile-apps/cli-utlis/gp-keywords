const globby = require("globby");
const kwplanner = require("../../src/google-kwplanner-processing");
const fs = require("fs-extra");
const sanitize = require("sanitize-filename");

exports.command = "filter <csvfile> [csvfiles..]";
exports.describe =
  "Парсит csv с ключами, экспортированные из Google Keyword Planner-а и фильтрует по критериям Competition и Avg. Monthly Searches. После этого полезно вручную убрать ключи, использующие бренды, чтобы потом сэкономить время на kwplanner check.";
exports.builder = yargs =>
  yargs
    .positional("csvfile", {
      describe:
        "csv-файл, экспортированный из Google Keyword Planner-а. Также принимает маски файлов (glob).",
      type: "string"
    })
    .positional("csvfiles", {
      describe: "аналогично csvfile"
    })
    .option("output", {
      alias: "o",
      describe:
        "Куда выгружать результаты; по-умолчанию - ./kwplanner-filtered-keywords.txt. Существующие файлы перезаписываются.",
      requiresArg: true,
      type: "string"
    })
    .option("num-tries", {
      alias: "n",
      describe:
        "Сколько раз нужно запустить проверку для каждого ключевого слова. По результатам проверок вычисляется медиана критериев, на основе которой принимается решение, является ли ключевое слово хорошим.",
      requiresArg: true,
      type: "number",
      default: 10
    });

exports.handler = async argv => {
  let filePaths = [argv.csvfile, ...argv.csvfiles];
  filePaths = await globby(filePaths);

  // парсим файлы
  const records = kwplanner.parseCSVFiles(filePaths);
  console.log(`Total keywords from Google Keyword Planner: ${records.length}`);

  // фильтруем ключи
  const keywords = records
    .filter(
      rec =>
        rec.keyword.split(" ").length >= 2 &&
        ["Low", "Низкий"].includes(rec.competition) &&
        rec.minVolume >= 10000 &&
        rec.maxVolume <= 1000000
    )
    .map(rec => rec.keyword);
  console.log(
    `Filtered keywords (2+ words, low competition, volume from 10K to 1M): ${keywords.length}`
  );

  // создадим или очистим уже существующий файл
  const keywordsStr = keywords.join("\n");
  let outputFilename;
  if (argv.output) {
    outputFilename = sanitize(argv.output);
  } else {
    outputFilename = "./kwplanner-filtered-keywords.txt";
  }
  fs.writeFileSync(outputFilename, keywordsStr);
};
