const globby = require("globby");
const kwplanner = require("../../src/google-kwplanner-processing");
const fs = require("fs-extra");
const sanitize = require("sanitize-filename");
const _ = require("lodash");

exports.command = "check <keywords-file> [keywords-files..]";
exports.describe =
  "Анализирует ключи и находит траффистые. Обязательно указывайте язык и гео!";
exports.builder = yargs =>
  yargs
    .positional("keywords-file", {
      describe:
        "Текстовый файл с ключевыми словами, по слову на каждой строке. Также принимает маски файлов (glob).",
      type: "string"
    })
    .positional("keywords-files", {
      describe: "аналогично keywords-file"
    })
    .option("resume", {
      alias: "r",
      describe:
        "Продолжает проверку ключевых слов из предыдущей сессии. Здесь указывается файл, в котором каждое ключевое слово находится на отдельной строке. Эти слова будут исключены из проверки, а новые будут дописаны в этот файл.",
      requiresArg: true,
      type: "string",
      conflicts: "output"
    })
    .option("output", {
      alias: "o",
      describe:
        "Куда выгружать результаты; по-умолчанию - ./kwplanner-result %y-%m-%d at %h_%m_%s.txt. Существующие файлы перезаписываются.",
      requiresArg: true,
      type: "string"
    });

exports.handler = async argv => {
  let filePaths = [argv.keywordsFile, ...argv.keywordsFiles];
  filePaths = await globby(filePaths);

  const keywordGroups = await Promise.all(
    filePaths.map(async filePath => {
      const buf = await fs.readFile(filePath);
      return buf
        .toString()
        .split("\n")
        .map(s => s.trim());
    })
  );
  let keywords = [].concat(...keywordGroups);

  // парсим файлы
  console.log(`Total keywords: ${keywords.length}`);

  // исключаем указанные ключевые слова (при наличии)
  if (argv.resume) {
    let exclude = fs.readFileSync(argv.resume.trim(), "utf8");
    exclude = exclude
      .split("\n")
      .map(line => line.trim())
      .filter(line => line.length > 0);
    keywords = _.difference(keywords, exclude);
    console.log(
      `Skipping ${exclude.length} keywords. Total keywords left to check: ${keywords.length}`
    );
  }

  // TODO заменить на парсинг подсказок
  // создадим или очистим уже существующий файл
  let outputFilename;
  if (argv.output) {
    outputFilename = sanitize(argv.output);
    fs.writeFileSync(outputFilename, "");
  } else if (argv.resume) {
    outputFilename = argv.resume;
  } else {
    const date = new Date();
    const pad = n => (n < 10 ? "0" + n : n);
    const Y = date.getFullYear();
    const M = pad(date.getMonth() + 1);
    const D = pad(date.getDate());
    const h = pad(date.getHours());
    const m = pad(date.getMinutes());
    const s = pad(date.getSeconds());
    outputFilename = `./kwplanner-result ${Y}-${M}-${D} at ${h}_${m}_${s}.txt`;
    fs.writeFileSync(outputFilename, "");
  }
  fs.ensureFileSync(outputFilename);

  // проверяем в ГП по критериям
  const keywordValidator = new kwplanner.KeywordValidator(10);
  const scraperOptions = {
    throttle: argv.throttle,
    gl: argv.gl,
    hl: argv.hl
  };

  for (let i = 0; i < keywords.length; i++) {
    const keyword = keywords[i];
    console.log(`[${i + 1}/${keywords.length}] Checking keyword "${keyword}"`);
    const timestampA = new Date();
    const isValid = await keywordValidator.validateKeyword(
      keyword,
      scraperOptions
    );
    const timestampB = new Date();
    console.log(
      `Is keyword good: ${isValid} (${(timestampB - timestampA) / 1000}s)`
    );

    // дописываем проверенное ключевое слово в выходной файл
    // TODO заменить на парсинг подсказок
    if (isValid) {
      fs.appendFileSync(outputFilename, keyword + "\n");
    }
  }
};
