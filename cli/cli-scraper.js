exports.command = "scraper <command>";
exports.description = "Google Play Scraper CLI";

exports.builder = yargs =>
  yargs
    .commandDir("cli-scraper")
    .demandCommand();

exports.handler = () => {};
