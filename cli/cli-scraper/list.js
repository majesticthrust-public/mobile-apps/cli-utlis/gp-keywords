const gp = require("google-play-scraper");
const fs = require("fs-extra");
const sanitize = require("sanitize-filename");
const ProgressBar = require("progress");

const gpConstants = require("google-play-scraper/lib/constants");

exports.command = "list";
exports.description =
  "Retrieve a list of apps that result from searching by the given term";

exports.builder = yargs =>
  yargs
    .option("collection", {
      description: "Collection; see available values",
      type: "string",
      choices: Object.keys(gpConstants.collection),
      default: "NEW_FREE",
      demandOption: true
    })
    .option("category", {
      description: "Category; see available values. Optional",
      type: "string",
      choices: Object.keys(gpConstants.category)
      // not required
    })
    .option("age", {
      description: "Age group; see available values. Optional",
      type: "string",
      choices: Object.keys(gpConstants.age)
      // not required
    })
    .option("num", {
      alias: "n",
      description: "How many apps to retrieve; [1..600]",
      type: "number",
      default: 500,
      coerce: num => {
        // clamp between 1 and 500
        num = Math.max(1, num);
        num = Math.min(600, num);
        return num;
      }
    })
    .option("fullDetail", {
      alias: "f",
      description:
        "If true, an extra request for each app will be made to fetch full details",
      type: "boolean",
      default: false
    })
    .option("output", {
      alias: "o",
      description:
        "Output file; if not specified, results will be saved to ./list-<collection>.json or ./list-<collection>-<category>.json",
      type: "string"
    });

exports.handler = async argv => {
  const params = {
    collection: gpConstants.collection[argv.collection],
    category: gpConstants.category[argv.category],
    age: gpConstants.age[argv.age],
    fullDetail: argv.fullDetail,

    throttle: argv.throttle,
    lang: argv.hl,
    country: argv.gl
  };

  const apps = [];
  let start = 0;
  let count = Math.min(argv.num, 100);

  const progress = new ProgressBar(
    "[:current/:total] [:bar] :percent; elapsed :elapseds, eta: :etas",
    {
      width: 30,

      // potentially invalid if the result count will be less than requested
      // but that won't happen, would it?
      total: Math.floor(argv.num / count)
    }
  );
  progress.tick(0);

  while (apps.length < argv.num) {
    const page = await gp.list(
      Object.assign(
        {
          num: count,
          start
        },
        params
      )
    );

    apps.push(...page);
    progress.tick();

    if (page.length < count) {
      break;
    } else {
      start += page.length;
      count = Math.min(count, argv.num - apps.length);
    }
  }

  let fileName = argv.output;
  if (!fileName) {
    if (argv.category) {
      fileName = `list-${argv.collection}-${argv.category}.json`;
    } else {
      fileName = `list-${argv.collection}.json`;
    }

    fileName = sanitize(fileName);
  }

  fs.outputJsonSync(fileName, apps, {
    spaces: 2
  });
};
