const gp = require("google-play-scraper");
const fs = require("fs-extra");
const sanitize = require("sanitize-filename");

exports.command = "search <query>";
exports.description =
  "Retrieve a list of apps that result from searching by the given term";

exports.builder = yargs =>
  yargs
    .positional("query", {
      description: "Search query"
    })
    .option("num", {
      alias: "n",
      description: "How many apps to retrieve; [1..250]",
      type: "number",
      default: 250
    })
    .option("fullDetail", {
      alias: "f",
      description:
        "If true, an extra request for each app will be made to fetch full details",
      type: "boolean",
      default: false
    })
    .option("price", {
      description: "What kind of apps to retrieve: free, paid, or both",
      type: "string",
      choices: ["free", "paid", "all"],
      default: "all"
    })
    .option("output", {
      alias: "o",
      description:
        "Output file; if not specified, results will be saved to ./<query>.json",
      type: "string"
    });

exports.handler = async argv => {
  // парсим поисковую выдачу
  let results = await gp.search({
    term: argv.query,
    num: argv.num,
    // TODO раскомментировать, когда либа начнет поддерживать эту опцию
    // fullDetail: argv.fullDetail,
    price: argv.price,

    throttle: argv.throttle,
    lang: argv.hl,
    country: argv.gl
  });

  if (argv.fullDetail) {
    // получаем детали для каждого приложения
    // TODO progressbar
    results = await Promise.all(
      results.map(r =>
        gp.app({
          appId: r.appId,

          throttle: argv.throttle,
          lang: argv.hl,
          country: argv.gl
        })
      )
    );
  }

  const fileName = argv.output || sanitize(`${argv.query}.json`);
  fs.outputJsonSync(fileName, results, { spaces: 2 });
};
