const gp = require("google-play-scraper");
const fs = require("fs-extra");
const ProgressBar = require("progress");

exports.command = "app [appIds..]";
exports.description =
  "Retrieve the full detail of an application (or list of applications)";

exports.builder = yargs =>
  yargs
    .positional("appIds", {
      description: "App ids to process"
    })
    .option("appList", {
      description: "File with a list of apps to process",
      type: "string"
    })
    .option("output", {
      alias: "o",
      description: "Output file",
      type: "string",
      default: "apps.json"
    });

exports.handler = async argv => {
  /**
   * @type {string[]}
   */
  const appIds = [];

  appIds.push(...argv.appIds);

  if (argv.appList) {
    const lines = fs.readFileSync(argv.appList)
      .toString()
      .split("\n")
      .map(line => line.trim())
      .filter(line => line.length > 0);

    appIds.push(...lines);
  }

  const progress = new ProgressBar("[:current/:total] [:bar] :percent; elapsed :elapseds, eta: :etas", {
    width: 30,
    total: appIds.length
  });

  const results = [];

  for (const appId of appIds) {
    const app = await gp.app({
      appId,
      lang: argv.hl,
      country: argv.gl,
      throttle: argv.throttle
    });

    results.push(app);
    progress.tick();
  }

  fs.outputJsonSync(argv.output, results);
};
