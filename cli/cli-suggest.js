const miss = require("mississippi");
const ProgressBar = require("progress");
const csvStringify = require("csv-stringify");
const fs = require("fs-extra");
const path = require("path");
const globby = require("globby");
const sanitize = require("sanitize-filename");
const {
  ScoredKeywordAccumulator,
  KeywordSuggestParser
} = require("../src/suggest");

exports.command = "suggest [keywords..]";
exports.describe =
  "Парсит подсказки для данного ключевика (-ов) и оценивает каждый на основе позиции в выдаче, способа получения подсказки, и глубине";
exports.builder = yargs =>
  yargs
    .option("inputFiles", {
      alias: "I",
      describe:
        "Файл(ы) со списком ключевых слов, по которым необходимо распасить подсказки. Поддерживает маски имен (glob).",
      type: "array",
      default: []
    })
    .option("depth", {
      alias: "d",
      describe: "Глубина парсинга подсказок",
      type: "number",
      default: 2
    })
    .option("outputDir", {
      alias: "O",
      description: "Папка для вывода результатов",
      conflicts: "merge",
      type: "string",
      default: "."
    })
    .option("outputScores", {
      description: "Выводить ли оценки ключевых слов",
      type: "boolean"
    })
    .option("addLetters", {
      describe: "При использовании данной опции будут собираться дополнительные ключи путем добавления букв и цифр к исходным ключам.",
      type: "boolean"
    })

    .option("weightKeyword", {
      describe: "Оценки: вес подсказок, полученных прямо из ключевика.",
      type: "number",
      default: 20
    })
    .option("weightKeywordSpace", {
      describe: "Оценки: вес подсказок, полученных из ключевика, дополненного пробелом.",
      type: "number",
      default: 2
    })
    .option("weightKeywordChar", {
      describe: "Оценки: вес подсказок, полученных из ключевика, дополненного символом.",
      type: "number",
      default: 1
    });

// .option("output", {
//   alias: "o",
//   describe: "Файл для вывода",
//   // conflicts: "keywords",
//   type: "string",
//   default: ""
// });

function processKeyword(
  keyword,
  parserOptions,
  accumulatorOptions,
  scraperOptions
) {
  console.log(`Processing "${keyword}"`);

  return new Promise(resolve => {
    const progress = new ProgressBar(
      "[:processedItems/:allItems] :customPercent%",
      {
        total: 1,
        width: 30,
        clear: true,
        callback: () => console.log()
      }
    );

    const parser = new KeywordSuggestParser(
      keyword,
      parserOptions,
      scraperOptions
    );

    const progressBarStream = miss.through.obj((chunk, enc, cb) => {
      progress.total = parser.totalItems;

      // обновляем токены
      progress.tick(0, {
        processedItems: parser.processedItems,
        allItems: parser.totalItems,

        // маленький хак для округления до сотых
        customPercent:
          Math.round((parser.processedItems / parser.totalItems) * 100 * 100) /
          100
      });

      cb(null, chunk);
    });

    const accumulator = new ScoredKeywordAccumulator(
      accumulatorOptions,
      scores => {
        // завершаем прогрессбар
        progress.tick(1);
        resolve(scores);
      }
    );

    parser.pipe(progressBarStream).pipe(accumulator);
  });
}

exports.handler = async argv => {
  const scraperOptions = {
    country: argv.gl,
    lang: argv.hl,
    throttle: 0 // убираем ограничение у скрейпера;
  };

  const parserOptions = {
    depth: argv.depth,
    addLetters: argv.addLetters,
    queueOptions: {
      concurrency: 200
    }
  };

  const accumulatorOptions = {
    keywordTypeWeights: {
      kw: argv.weightKeyword,
      kwspace: argv.weightKeywordSpace,
      kwchar: argv.weightKeywordChar
    }
  };

  const seedKeywords = [];

  // читаем содержимое указанных входных файлов
  const filePaths = await globby(argv.inputFiles);
  for (const filePath of filePaths) {
    const keywords = fs
      .readFileSync(filePath, "utf8")
      .split("\n")
      .map(line => line.trim())
      .filter(line => line.length > 0);

    seedKeywords.push(...keywords);
  }

  // const seedKeywords = [].concat(argv.keyword).concat(argv.keywords);
  if (argv.keywords) {
    seedKeywords.push(...argv.keywords);
  }

  // TODO поддержка нескольких файлов и суммирование их результатов
  for (const keyword of seedKeywords) {
    const scores = await processKeyword(
      keyword,
      parserOptions,
      accumulatorOptions,
      scraperOptions
    );

    if (scores.length === 0) {
      console.log(`Keyword "${keyword}" has no suggestions, ignoring...`);
      continue;
    }

    if (argv.outputScores) {
      csvStringify(
        scores,
        {
          header: true,
          columns: [
            { key: "keyword", header: "Keyword" },
            { key: "score", header: "Score" }
          ]
        },
        (err, str) => {
          if (err) {
            console.error(err);
            return;
          }

          // const fname = sanitize(argv.output ? argv.output : `${keyword}.csv`);
          let fname = sanitize(`${keyword}.csv`);
          fname = path.resolve(argv.outputDir, fname);
          fs.outputFile(fname, str);
        }
      );
    } else {
      const str = scores.map(s => s.keyword).join("\n");
      let fname = sanitize(`${keyword}.txt`);
      fname = path.resolve(argv.outputDir, fname);
      fs.outputFile(fname, str);
    }
  }
};
