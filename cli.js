#!/usr/bin/env node

require("yargs")
  .option("gl", {
    describe: "Geographical location 2-letter code",
    requiresArg: true,
    type: "string",
    default: "us",
    global: true
  })
  .option("hl", {
    describe: "Language 2-letter code",
    requiresArg: true,
    type: "string",
    default: "en",
    global: true
  })
  .option("throttle", {
    description: "How many requests per second can be made",
    requiresArg: true,
    type: "number",
    default: 10,
    global: true
  })
  .commandDir("cli")
  .demandCommand()
  .help()
  .argv;
