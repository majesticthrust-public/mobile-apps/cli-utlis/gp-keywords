const gp = require("google-play-scraper");
const { Readable, Writable } = require("stream");
const PQueue = require("p-queue");
const _ = require("lodash");
const alphabets = require("./alphabets");
const { defaultParams } = require("./util");

/**
 * Ключевое слово, только что распаршенное из подсказок и готовое к вычислению оценки.
 * @typedef {Object} KeywordNode
 * @property {string} keyword - ключевое слово
 * @property {("kw"|"kwspace"|"kwchar")} type - тип ключевого слова (исходя из типа ключа, из которого был получен данный ключ)
 * @property {number} position - позиция в выдаче
 * @property {number} totalItems - количество ключей в выдаче
 * @property {number} treeDepth - глубина ключа в дереве подсказок; 0 - корень, 1 - подсказки корня, 2 - подсказки подсказок, и т.д.
 */

/**
 * Ключевое слово с оценкой по поисковым подсказкам.
 * @typedef {Object} KeywordScore
 * @property {string} keyword
 * @property {number} score
 */

/**
 * Парсер подсказок по ключевому слову. Парсит все подряд и возвращает вместе с дубликатами.
 * Подразумевается, что результаты будут где-то сохраняться, рассчитывать оценки и суммироваться.
 */
class KeywordSuggestParser extends Readable {
  /**
   * Параметры для внутренней очереди, позволяющие управлять ограничениями на количество одновременно выполняемых задач
   * @typedef {Object} QueueOptions
   * @property {number} concurrency - сколько задач может выполняться одновременно
   * @property {number} intervalCap - количество задач, которые могут выполниться в течение заданного промежутка времени
   * @property {number} interval - промежуток времени в миллисекундах для intervalCap
   */

  /**
   * Параметры для парсера подсказок
   * @typedef {Object} ParserOptions
   * @property {number} depth - глубина, на которую необходимо распарсить подсказки
   * @property {bool} addLetters - добавлять ли буквы и цифры в процессе сбора подсказок
   * @property {QueueOptions} queueOptions - параметры для внутренней очереди
   */

  /**
   * @param {string} keyword - ключевое слово, которое необходимо распарсить
   * @param {ParserOptions} parserOptions - параметры для данного парсера
   * @param {Object} scraperOptions - параметры для google-play-scraper
   */
  constructor(keyword, parserOptions, scraperOptions = {}) {
    super({
      objectMode: true
    });

    this.keyword = keyword;
    this.depth = parserOptions.depth;
    this.addLetters = parserOptions.addLetters || false;

    this.scraperOptions = defaultParams(scraperOptions);
    this.alphabet = alphabets[this.scraperOptions.lang];


    const queueOptions = _.defaults(
      Object.assign(parserOptions.queueOptions, {
        autoStart: false
      }),
      {
        concurrency: 100
      }
    );
    // чтобы очередь не ломалась чужими реализациями
    delete queueOptions.queueClass;

    /**
     * Очередь ключевиков на парсинг
     */
    this.queue = new PQueue(queueOptions);

    this.queue.add(() => this._processKeyword(this.keyword, 1));

    // конец потока
    this.queue.onIdle().then(() => {
      this.processedItems = this.totalItems;
      this._pushResults([null]);
    });

    /**
     * Внутреннее хранилище результатов; опустошается при чтении
     * @type {KeywordNode}
     */
    this._resultsBuffer = [];

    /**
     * Количество элементов, запрошенное в _read()
     */
    this._requestedReadSize = 0;
    /**
     * Количество элементов, прочитанных из потока чтения
     */
    this._itemsRead = 0;

    /**
     * Количество ключевиков, которое необходимо обработать. Значение изначально равно 1 (начальное ключевое слово) и изменяется по мере работы.
     */
    this.totalItems = 1;

    /**
     * Количество обработанных ключевиков.
     */
    this.processedItems = 0;

    /**
     * Количество ошибок скрейпера, возникших при сборе подсказок
     */
    this.errorCount = 0;
  }

  /**
   * Обрабатывает ключевое слово на данной глубине. Парсит подсказки, выталкивает результаты, и создает новые задачи в очереди.
   * @param {string} keyword
   * @param {number} depth
   */
  async _processKeyword(keyword, depth) {
    try {
      const keywordNodes = await this._parseSuggestions(keyword, depth);
      this._pushResults(keywordNodes);

      this.processedItems++;

      if (depth < this.depth) {
        // добавляем новые ключи в очередь
        keywordNodes.forEach(kwnode => {
          this.queue.add(() =>
            this._processKeyword(kwnode.keyword, kwnode.treeDepth + 1)
          );

          this.totalItems++;
        });
      }
    } catch (e) {
      this.errors++;

      // в случае ошибки добавляем ключ в очередь заново
      this.queue.add(() => {
        this._processKeyword(keyword, depth);
      });
    }
  }

  /**
   * Добавляет обработанные ключевики во внутреннее хранилище.
   * При этом пытается опустошить это хранилище в очередь потока чтения.
   * @param {KeywordScore[]} keywordNodes
   */
  _pushResults(keywordNodes) {
    this._resultsBuffer.push(...keywordNodes);

    while (
      this._itemsRead < this._requestedReadSize &&
      this._resultsBuffer.length > 0
    ) {
      const item = _.head(this._resultsBuffer);

      if (this.push(item)) {
        this._resultsBuffer = _.tail(this._resultsBuffer);
      } else {
        break;
      }
    }

    // ставим очередь на паузу, если прочитали все, что запросили
    if (this._itemsRead === this._requestedReadSize) {
      this.queue.pause();
    }
  }

  /**
   * Парсит подсказки
   * @param {string} keyword - ключевое слово
   * @param {number} depth - глубина получаемых подсказок в дереве подсказок; 1 - подсказки корня, 2 - подсказки подсказок
   * @returns {Promise<KeywordNode[]>}
   */
  async _parseSuggestions(keyword, depth) {
    const suggestions = [];

    // парсинг ключа
    suggestions.push(
      await this._suggest(keyword, {
        type: "kw",
        treeDepth: depth
      })
    );

    if (this.addLetters) {
      // парсинг ключа с пробелом
      suggestions.push(
        await this._suggest(keyword + " ", {
          type: "kwspace",
          treeDepth: depth
        })
      );

      // парсинг ключа с буквами и цифрами
      for (const letter of this.alphabet) {
        suggestions.push(
          await this._suggest(keyword + " " + letter, {
            type: "kwspace",
            treeDepth: depth
          })
        );
      }
    }

    return _.flatten(suggestions);
  }

  /**
   * Парсит подсказки и преобразует их в KeywordNode-ы
   * @param {string} keyword
   * @param {Object} nodeData
   * @param {("kw"|"kwspace"|"kwchar")} nodeData.type
   * @param {number} nodeData.treeDepth
   * @returns {Promise<KeywordNode[]>}
   */
  async _suggest(keyword, nodeData) {
    const suggestions = await gp.suggest(
      Object.assign(this.scraperOptions, { term: keyword })
    );
    return suggestions.map((suggestion, i) =>
      Object.assign(
        {
          keyword: suggestion,
          position: i + 1,
          totalItems: suggestions.length
        },
        nodeData
      )
    );
  }

  _read(size) {
    // возобновляем работу очереди при новом запросе чтения
    this._requestedReadSize = size;
    this._itemsRead = 0;
    this.queue.start();
  }
}

class ScoredKeywordAccumulator extends Writable {
  /**
   * Веса для разных подсказок, происходящих из разных дополнений ключа.
   * @typedef {Object} KeywordTypeWeights
   * @property {number} kw - подсказка, полученная напрямую из ключа
   * @property {number} kwspace - подсказка, полученная из ключа, дополненного пробелом
   * @property {number} kwchar - подсказка, полученная из ключа, дополненного буквой/цифрой
   */

  /**
   * Параметры для аккумулятора оценок ключей
   * @typedef {Object} KeywordScoreAccumulatorOptions
   * @property {KeywordTypeWeights} keywordTypeWeights
   */

  /**
   * @callback KeywordScoreAccumulatorCallback
   * @param {KeywordScore[]} keywordScores
   * @returns {void}
   */

  /**
   * @param {KeywordScoreAccumulatorOptions} accumulatorOptions
   * @param {KeywordScoreAccumulatorCallback} doneCallback
   */
  constructor(accumulatorOptions, doneCallback) {
    super({
      objectMode: true
    });

    this.keywordTypeWeights = accumulatorOptions.keywordTypeWeights;
    this.doneCallback = doneCallback;

    /**
     * Словарь, где ключи - нормализованные ключевые слова, а значения - KeywordScore
     */
    this.keywordScores = {};
  }

  /**
   * Рассчитывает оценку ключевого слова
   * @param {KeywordNode} keywordNode
   * @returns {KeywordScore}
   */
  _calculateScore(keywordNode) {
    const { keyword, type, position, totalItems, treeDepth } = keywordNode;
    const typeWeight = this.keywordTypeWeights[type];
    const positionWeight = totalItems - position + 1;
    const treeDepthWeight = 1 / treeDepth;

    return {
      keyword,
      score: typeWeight * positionWeight * treeDepthWeight
    };
  }

  /**
   * Накапливает оценку
   * @param {KeywordScore} keywordScore
   */
  _accumulateScore(keywordScore) {
    const normalizedKeyword = keywordScore.keyword.toLowerCase();
    const savedScore = this.keywordScores[normalizedKeyword];

    if (savedScore === undefined) {
      // создаем новую запись
      keywordScore.keyword = normalizedKeyword;
      this.keywordScores[normalizedKeyword] = keywordScore;
    } else {
      // суммируем оценку
      savedScore.score += keywordScore.score;
    }
  }

  _write(chunk, encoding, next) {
    const score = this._calculateScore(chunk);
    this._accumulateScore(score);
    next();
  }

  _writev(chunks, next) {
    chunks
      .map(this._calculateScore.bind(this))
      .forEach(this._accumulateScore.bind(this));
    next();
  }

  _final(cb) {
    // преобразуем словарь в список
    const results = _.values(this.keywordScores);
    this.doneCallback(results);
    cb();
  }
}

/**
 * Парсит подсказки к данному ключевому слову с данными параметрами
 * @param {string} keyword - ключевое слово, которое необходимо распарсить
 * @param {ParserOptions} parserOptions - параметры для парсера
 * @param {Object} scraperOptions - параметры для google-play-scraper
 * @returns {Promise<KeywordScore[]>}
 */
function parseKeywordSuggestions(keyword, parserOptions, scraperOptions) {
  return new Promise(resolve => {
    const parser = new KeywordSuggestParser(
      keyword,
      parserOptions,
      scraperOptions
    );
    const accumulator = new ScoredKeywordAccumulator(resolve);
    parser.pipe(accumulator);
  });
}

exports.KeywordSuggestParser = KeywordSuggestParser;
exports.ScoredKeywordAccumulator = ScoredKeywordAccumulator;
exports.parseKeywordSuggestions = parseKeywordSuggestions;
