const _ = require("lodash");
const XRegExp = require("xregexp");

// keyword density
//
// params:
// - min keyword length
// - max words
// - app [title, short description, full description]
//
// output:
// - keywords [keyword, frequency]

/**
 * Преобразует данный список в список всех возможных кортежей из N подряд идущих элементов.
 * @param {any[]} list
 * @param {number} N
 * @returns {any[][]}
 */
function generateTuplesFromList(list, N) {
  const tuples = [];

  if (list.length === N) {
    tuples.push(_.slice(list));
  }

  for (let i = 0; i < list.length - N; i++) {
    const tuple = _.slice(list, i, i + N);
    tuples.push(tuple);
  }

  return tuples;
}

/**
 * Подсчитывает частоту появлений разных элементов в списке
 * @param {any[]} list
 * @returns {{element: any, frequency: number}[]}
 */
function calculateDensity(list) {
  const dict = {};
  for (const item of list) {
    if (dict[item] === undefined) {
      dict[item] = 1;
    } else {
      dict[item]++;
    }
  }

  const result = [];
  for (const key of Object.keys(dict)) {
    result.push({
      element: key,
      frequency: dict[key]
    });
  }

  return result;
}

/**
 * Вычленяет из данного текста все возможные ключевые слова до определенной длины. Не включает ключевые слова, содержащие слова длиной меньше заданной.
 * @param {string} text текст, из которого необходимо вычленить ключевые слова
 * @param {Object} options параметры
 * @param {number} options.maxWords максимальное количество слов в ключе
 */
// TODO сделать так же, как в TheTool Keyword Density, но при этом не выбрасывать ключевики, встретившиеся один раз
function keywordDensity(text, options = {}) {
  _.defaults(options, {
    maxWords: 4
  });

  const pattern = new XRegExp("[\\s\\p{P}]+");
  const words = XRegExp.split(text, pattern).map(word => word.toLowerCase());

  let keywords = [];

  _.range(options.maxWords).forEach(i => {
    const tupleLength = i + 1;
    // создадим все возможные кортежи из идущих подряд слов
    const tuples = generateTuplesFromList(words, tupleLength);
    keywords = _.concat(keywords, tuples);
  });

  // преобразуем кортежи в фразы
  keywords = keywords.map(tuple => tuple.join(" "));
  return calculateDensity(keywords);
}

exports.keywordDensity = keywordDensity;
