const csvparse = require("csv-parse/lib/sync");
const fs = require("fs-extra");
const _ = require("lodash");
const gp = require("google-play-scraper");
const util = require("./util");

/**
 * @typedef {Object} KeywordData
 * @property {string} keyword
 * @property {string} competition
 * @property {number} minVolume
 * @property {number} maxVolume
 */

/**
 * Читает и парсит csv-файл, импортированный из Google Keyword Planner-а.
 * Отсекает лишние столбцы и возвращает строки в виде объектов {@link KeywordData}.
 * @param {string} filePath
 * @returns {KeywordData[]}
 */
function parseCSV(filePath) {
  let contents = fs.readFileSync(filePath, "utf16le");

  // пропустим первые две строки (в них мусор)
  let i = contents.indexOf("\n", 0);
  i = contents.indexOf("\n", i + 1);
  contents = contents.substring(i + 1);

  const records = csvparse(contents, {
    delimiter: "\t",
    columns: true
  }).map(r => ({
    keyword: r["Keyword"],
    competition: r["Competition"],
    minVolume: parseInt(r["Min search volume"].replace(/,/g, ""), 10),
    maxVolume: parseInt(r["Max search volume"].replace(/,/g, ""), 10)
  }));

  return records;
}

/**
 * Парсит все указанные csv-файлы, импортированные из Google Keyword Planner-а.
 * Отсекает лишние столбцы и возвращает строки в виде объектов {@link KeywordData}.
 * Удаляет дубликаты.
 * @param {string[]} filePaths
 * @returns {KeywordData[]}
 */
function parseCSVFiles(filePaths) {
  const allRecords = [];
  for (const filePath of filePaths) {
    try {
      const records = parseCSV(filePath);
      allRecords.push(...records);
    } catch (e) {
      console.error(e);
    }
  }

  return _.uniqBy(allRecords, "keyword");
}

/**
 * @typedef {Object} ScraperOptions
 * @property {string} gl
 * @property {string} hl
 * @property {number} throttle
 */

class KeywordValidator {
  /**
   * @param {number} numTries сколько раз повторить оценку перед усреднением результатов.
   */
  constructor(numTries) {
    if (_.isUndefined(numTries)) {
      throw new Error("numTries is required!");
    }

    if (numTries <= 0) {
      throw new Error(`numTries has invalid value: ${numTries}`);
    }

    this.numTries = numTries;
  }

  /**
   * Проверяет ключевое слово в Google Play по следующим критериям:
   * - количество прямых вхождений ключевого слова в названия приложений в поисковой выдаче не должно превышать определенного количества для Tier-1, Tier-2, Tier-3 стран;
   * - Среднее количество инсталлов в топ-10 приложений не старше года должно быть 10к и выше;
   * Проверяет ключ заданное при создании класса количество раз, после чего усредняет результаты.
   * @param {string} keyword
   * @param {ScraperOptions} scraperOptions
   * @returns {Promise<boolean>} Является ли ключевое слово подходящим для работы с органикой.
   * @public
   */
  async validateKeyword(keyword, scraperOptions) {
    const scores = [];
    for (let i = 0; i < this.numTries; i++) {
      // TODO сделать коллекцию, которая будет загружать информацию
      // о приложениях по мере необходимости. Т.е. при создании будет
      // загружать поверхностную инфу о приложениях, а о каждом отдельном
      // - при обращении по соответствующему индексу. Это позволит сильно
      // оптимизировать рассчет оценок. Оценки нужно будет соответствующим
      // образом переделать, чтобы использовать оптимизацию.
      const searchResults = await this._getSearchResults(
        keyword,
        scraperOptions
      );
      const score = {
        criteria1: this._criteria1(keyword, searchResults),
        criteria2: await this._criteria2(searchResults, scraperOptions)
      };
      console.log(
        `Try ${i}\t` +
          `Title occurences: ${score.criteria1}\t` +
          `Mean top10 installs: ${score.criteria2}`
      );
      scores.push(score);
    }

    // усредняем оценки
    // const criteria1Score = Math.round(
    //   _.sumBy(scores, s => s.criteria1) / scores.length
    // );
    // const criteria2Score = Math.round(
    //   _.sumBy(scores, s => s.criteria2) / scores.length
    // );
    const criteria1Score = util.median(scores.map(s => s.criteria1));
    const criteria2Score = util.median(scores.map(s => s.criteria2));
    console.log(
      `Median scores: title occurences = ${criteria1Score}, installs = ${criteria2Score}`
    );

    return (
      this._validateCriteria1(criteria1Score, scraperOptions.gl) &&
      this._validateCriteria2(criteria2Score)
    );
  }

  /**
   * Поиск по Google Play с full detail.
   * @param {string} keyword
   * @param {Object} scraperOptions
   * @returns {Promise<Object[]>}
   */
  async _getSearchResults(keyword, scraperOptions) {
    let searchResults;

    try {
      searchResults = await gp.search({
        term: keyword,
        num: 250,
        // fullDetail: true,
        throttle: scraperOptions.throttle,
        lang: scraperOptions.hl,
        country: scraperOptions.gl
      });

      // TODO выпилить этот костыль, когда google-play-scraper начнет поддерживать fullDetail в search
      // searchResults = await Promise.all(
      //   searchResults.map(r =>
      //     gp.app({
      //       appId: r.appId,

      //       throttle: scraperOptions.throttle,
      //       lang: scraperOptions.hl,
      //       country: scraperOptions.gl
      //     })
      //   )
      // );

      return searchResults;
    } catch (e) {
      console.error(`Error while parsing search: ${e}; retrying...`);
      return this._getSearchResults(keyword, scraperOptions);
    }
  }

  /**
   * Оценка по критерию 1: количество точных вхождений ключевого слова в названия приложений в поисковой выдаче.
   * @param {string} keyword
   * @param {Object[]} searchResults
   * @returns {number}
   * @private
   */
  _criteria1(keyword, searchResults) {
    const occurenceCount = searchResults.filter(r => r.title.includes(keyword))
      .length;

    return occurenceCount;
  }

  /**
   * Критерий 1: количество точных вхождений ключевого слова в названия приложений в поисковой выдаче не должно превышать определенного количества для Tier-1, Tier-2, Tier-3 стран.
   * @param {number} occurenceCount
   * @param {string} gl
   * @returns {boolean}
   * @private
   */
  _validateCriteria1(occurenceCount, gl) {
    let maxOccurences;
    switch (util.countryCodeToTier(gl)) {
      case 1:
        maxOccurences = 1;
        break;
      case 2:
        maxOccurences = 3;
        break;
      default:
        maxOccurences = Infinity;
    }

    return occurenceCount <= maxOccurences;
  }

  /**
   * Оценка по критерию 2: среднее количество инсталлов в топ-10 приложениях не старше года.
   * @param {Object[]} searchResults
   * @returns {Promise<number>}
   * @private
   */
  async _criteria2(searchResults, scraperOptions) {
    const now = Date.now();
    const top10young = [];

    for (const searchResult of searchResults) {
      const appDetails = await gp.app({
        appId: searchResult.appId,
        throttle: scraperOptions.throttle,
        lang: scraperOptions.hl,
        country: scraperOptions.gl
      });

      const diff = now - new Date(appDetails.released); // milliseconds
      const years = diff / (1000 * 60 * 60 * 24 * 365); // milliseconds to years
      // console.log(`App ${appDetails.appId}, date ${appDetails.released}, diff ${years} years`);

      if (years <= 1) {
        top10young.push(appDetails);
      }

      if (top10young.length == 10) {
        break;
      }
    }

    // console.log(`has 10 young apps: ${top10young.length >= 10}`);
    // if (top10young.length < 10) return false;

    const installs = top10young.map(app => app.minInstalls);
    // const averageInstalls = installs ? _.sum(installs) / installs.length : 0;
    const averageInstalls = installs ? util.median(installs) : 0;

    return averageInstalls;
  }

  /**
   * Критерий 2: среднее количество инсталлов в топ-10 приложений не старше года должно быть 10к и выше.
   * @returns {boolean}
   * @private
   */
  _validateCriteria2(averageInstalls) {
    return averageInstalls >= 10000;
  }
}

exports.parseCSV = parseCSV;
exports.parseCSVFiles = parseCSVFiles;
exports.KeywordValidator = KeywordValidator;
