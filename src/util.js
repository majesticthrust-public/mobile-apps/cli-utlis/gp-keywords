const _ = require("lodash");
const fs = require("fs-extra");
const miss = require("mississippi");
const split = require("split");

/**
 * Оборачивает Promise так, чтобы оно всегда выполнялось
 * @param {Promise<any>} promise
 * @param {any} errorValue с каким значением выполниться, если возникнет ошибка
 */
exports.wrapPromise = function wrapPromise(promise, errorValue = null) {
  return new Promise(resolve => {
    promise.then(resolve).catch(err => {
      console.error(err);
      resolve(errorValue);
    });
  });
};

/**
 * Параметры по-умолчанию для google-play-scraper
 */
exports.defaultParams = function defaultParams(params) {
  return _.defaults(params, {
    lang: "en",
    country: "us",
    throttle: 10
  });
};

/**
 * Подсчитывает количество строк в файле
 * @param {string} filePath путь к файлу
 * @param {any} linePredicate предикат, определяющий, нужно ли считать строку
 * @returns {Promise<number>}
 */
exports.countLinesInFile = function countLinesInFile(filePath, linePredicate) {
  const input = fs.createReadStream(filePath);

  let count = 0;
  const counter = miss.to.obj((chunk, enc, cb) => {
    const line = chunk.toString();
    if (linePredicate(line)) count++;
    cb();
  });

  input.pipe(split()).pipe(counter);

  return new Promise((resolve, reject) => {
    miss.finished(counter, err => (err ? reject(err) : resolve(count)));
  });
};

/**
 * Преобразует ISO-код страны в Tier.
 * @param {string} countryCode
 * @returns {number} 1, 2 или 3 (соответственно Tier-у)
 */
exports.countryCodeToTier = function countryCodeToTier(countryCode) {
  countryCode = countryCode.toLowerCase();

  // prettier-ignore
  const tier1codes = [
    "ca", "dk", "fi", "fr", "de", "ie", "it", "lu", "nl", "nz",
    "no", "es", "se", "ch", "gb", "us"
  ];

  // prettier-ignore
  const tier2codes = [
    "ad", "ar", "bs", "by", "bo", "ba", "br", "bn", "bg", "cl",
    "cn", "co", "cr", "hr", "cy", "cz", "do", "ec", "eg", "ee",
    "fj", "gr", "gy", "hk", "hu", "is", "id", "il", "jp", "kz",
    "lv", "lt", "mo", "my", "mt", "mx", "me", "ma", "np", "om",
    "pa", "py", "pe", "ph", "pl", "pt", "pr", "qa", "kr", "ro",
    "ru", "sa", "rs", "sg", "sk", "si", "za", "th", "tr", "ua",
    "ae", "uy", "vu"
  ];

  if (tier1codes.includes(countryCode)) {
    return 1;
  } else if (tier2codes.includes(countryCode)) {
    return 2;
  } else {
    return 3;
  }
};

/**
 * Медиана.
 * @param {number[]} distribution
 * @returns {number}
 */
exports.median = function median(distribution) {
  distribution = _.sortBy(distribution);
  const center = Math.floor(distribution.length / 2);

  if (distribution.length % 2 === 1) {
    return distribution[center];
  } else {
    return (distribution[center] + distribution[center - 1]) / 2;
  }
};
