const gp = require("google-play-scraper");
const _ = require("lodash");
const { Duplex } = require("stream");
const PQueue = require("p-queue");
const { wrapPromise, defaultParams } = require("./util");

/**
 * Возвращает позицию приложения в поисковой выдаче по данному запросу
 * @param {string} appId - id приложения в формате com.X.Y (то, что принимает гугл)
 * @param {string} searchTerm - поисковой запрос
 * @returns {Promise<number>} - позиция приложения в поисковой выдаче, [1..250]. Если результат = 0, то приложения нет в поисковой выдаче.
 */
async function getAppPosition(appId, searchTerm, params = {}) {
  params = defaultParams(params);
  params = Object.assign(params, {
    term: searchTerm,
    num: 250
  });

  return gp
    .search(params)
    .then(result => result.findIndex(app => app.appId == appId) + 1);
}

/**
 * Возвращает список позиций данных приложений в поисковой выдаче по данному запросу
 * @param {string[]} appIds
 * @param {string} searchTerm
 * @param {} params
 * @returns {Promise<{appId: string, position: number}[]>}
 */
async function getAppsPosition(appIds, searchTerm, params = {}) {
  params = defaultParams(params);
  params = Object.assign(params, {
    term: searchTerm,
    num: 250
  });

  const searchResults = await gp.search(params);
  return appIds.map(appId => ({
    appId,
    position: searchResults.findIndex(app => app.appId == appId) + 1
  }));
}

/**
 * Возвращает позиции приложения по данным поисковым запросам
 * @param {string} appId - id приложения в формате com.X.Y (то, что принимает гугл)
 * @param {string[]} searchTerms - поисковые запросы
 * @returns {Promise<{term: string, position: number, isError: boolean, isFound: boolean}[]>} позиции приложения в поисковой выдаче. Отдает список из результатов {@link getAppPosition}
 */
async function getAppPositions(appId, searchTerms, params) {
  const promises = searchTerms
    .map(term => getAppPosition(appId, term, params))
    .map(promise => wrapPromise(promise, -1)); // при ошибках позиция = -1; когда не найдено, то 0

  return Promise.all(promises).then(results =>
    _.zip(searchTerms, results).map(([term, result]) => ({
      term,
      position: result,
      isError: result === -1,
      isFound: result > 0
    }))
  );
}

/**
 * Проверка позиций данного приложения в поисковой выдаче Google Play.
 * Поток, преобразующий строки поисковых запросов в позиции.
 * Запросы никак не изменяются и не фильтруются, и передаются "как есть".
 */
// TODO обобщить, чтобы можно было делать другие штуки, требующие повторного опроса
class AppSearchPositionDuplex extends Duplex {
  /**
   * @typedef {{keyword: string, results: {appId: string, position: number, isFound: boolean}}} CheckerResult
   */

  /**
   * Создание потока, проверяющего позиции данных приложений
   * @param {string[]} packageNames названия приложений в формате com.X.Y
   * @param {{country: string, lang: string}} params параметры для парсера
   */
  constructor(packageNames, params) {
    super({
      decodeStrings: false,
      // readableObjectMode: true,
      // writableObjectMode: false
      objectMode: true
    });

    this.packageNames = packageNames;

    const throttleLimit = 10;
    this.params = Object.assign(params, { throttle: throttleLimit });

    // TODO вынести в параметры? нужно ли это вообще делать?
    this.queue = new PQueue({
      concurrency: throttleLimit,
      intervalCap: throttleLimit,
      interval: 1000,
      autoStart: false
    });

    /**
     * Буфер для результатов
     * @type {CheckerResult[]}
     */
    this._resultsBuffer = [];

    // debug data
    this._errors = 0;
    this._totalItems = 0;
  }

  /**
   * Добавляет поисковой запрос в очередь на обработку.
   * При успешной обработке добавляет результат в очередь готовых результатов.
   * При ошибке снова добавляет запрос в очередь на повторную обработку.
   * @param {string} keyword
   */
  _enqueueKeyword(keyword) {
    return this.queue.add(async () => {
      try {
        const positions = await getAppsPosition(
          this.packageNames,
          keyword,
          this.params
        );

        this._pushResult({
          keyword,
          results: positions.map(pos => ({
            appId: pos.appId,
            position: pos.position,
            isFound: pos.position > 0
          }))
        });
      } catch (e) {
        this._errors++;
        this._enqueueKeyword(keyword);
      }
    });
  }

  /**
   * Добавляет результат проверки во внутреннюю очередь;
   * При этом по мере возможности старается опустошить внутреннюю очередь в очередь потока чтения.
   * @param {CheckerResult} result
   */
  _pushResult(result) {
    this._resultsBuffer.push(result);

    // const prevLength = this._resultsBuffer.length;
    // console.log(`Pushing... resultsBuffer.length = ${prevLength}`);

    while (this._resultsBuffer.length > 0) {
      const item = _.head(this._resultsBuffer);

      if (this.push(item)) {
        this._resultsBuffer = _.tail(this._resultsBuffer);
      } else {
        break;
      }
    }

    // const newLength = this._resultsBuffer.length;
    // console.log(`Pushed ${prevLength - newLength} items. resultsBuffer.length = ${newLength}`);
  }

  _write(chunk, enc, next) {
    const keyword = chunk.toString();
    this._totalItems++;

    this._enqueueKeyword(keyword);
    this.queue.onEmpty().then(next);
  }

  _read() {
    this.queue.start();
  }

  _final(cb) {
    // ждем, когда обработаются все ключи
    this.queue.onIdle().then(() => {
      // конец данных для чтения
      this._pushResult(null);

      // TODO proper logging
      // console.log(`Total items: ${this._totalItems}; Errors: ${this._errors}`);

      cb();
    });
  }
}

exports.AppSearchPositionDuplex = AppSearchPositionDuplex;
