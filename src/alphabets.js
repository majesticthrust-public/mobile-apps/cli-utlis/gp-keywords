const alphabets = (() => {
  // если у нескольких языков один алфавит, в ключе
  // следует указать коды языков, разделенные пробелами
  // они автоматически развернутся
  const rawAlphabets = {
    ru: "абвгдеёжзийклмнопрстуфхцчшщъыьэюя",
    en: "abcdefghijklmnopqrstuvwxyz"
  };

  const numbers = "1234567890";

  for (const key of Object.keys(rawAlphabets)) {
    // строки с алвафитами в списки
    const alphabet = (rawAlphabets[key] + numbers).split("");
    const subkeys = key.split(" ");

    delete rawAlphabets[key];

    for (const subkey of subkeys) {
      rawAlphabets[subkey] = alphabet;
    }
  }

  return rawAlphabets;
})();

module.exports = alphabets;
