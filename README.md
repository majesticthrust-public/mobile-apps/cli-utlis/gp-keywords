# gp-keywords

Утилита для парсинга ключевых слов из Google Play.

## Установка

Установка последней версии:
```
npm install -g gitlab:majesticthrust-mobile-apps/cli-utils/gp-keywords
```

Установка конкретной версии:
```
npm install -g gitlab:majesticthrust-mobile-apps/cli-utils/gp-keywords#semver:^version_tag
```

`version_tag` необходимо заменить на соответствующую версию. Например:
```
npm install -g gitlab:majesticthrust-mobile-apps/cli-utils/gp-keywords#semver:^1.0.0
```
